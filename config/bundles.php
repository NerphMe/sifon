<?php

return [
    Symfony\Bundle\FrameworkBundle\FrameworkBundle::class => ['all.html.twig' => true],
    Symfony\Bundle\WebServerBundle\WebServerBundle::class => ['dev' => true],
    Symfony\Bundle\TwigBundle\TwigBundle::class => ['all.html.twig' => true],
    Symfony\Bundle\WebProfilerBundle\WebProfilerBundle::class => ['dev' => true, 'test' => true],
    Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle::class => ['all.html.twig' => true],
    Doctrine\Bundle\DoctrineBundle\DoctrineBundle::class => ['all.html.twig' => true],
    Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle::class => ['all.html.twig' => true],
    Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle::class => ['dev' => true, 'test' => true],
    FOS\UserBundle\FOSUserBundle::class => ['all' => true],
    Symfony\Bundle\MakerBundle\MakerBundle::class => ['dev' => true],
    Symfony\Bundle\SecurityBundle\SecurityBundle::class => ['all.html.twig' => true],
    A2lix\AutoFormBundle\A2lixAutoFormBundle::class => ['all.html.twig' => true],
    A2lix\TranslationFormBundle\A2lixTranslationFormBundle::class => ['all.html.twig' => true],
    Knp\DoctrineBehaviors\Bundle\DoctrineBehaviorsBundle::class => ['all.html.twig' => true],
    Nelmio\CorsBundle\NelmioCorsBundle::class => ['all.html.twig' => true],
];
