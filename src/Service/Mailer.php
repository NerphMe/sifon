<?php
declare(strict_types=1);

namespace App\Service;

use App\Entity\Users;
use Swift_Mailer;
use Swift_Message;
use Twig_Environment;

class Mailer
{
    public const FROM_ADDRESS = 'dr0n74085@gmail.com';

    /**
     * @var Swift_Mailer
     */
    private $mailer;

    /**
     * @var Twig_Environment
     */
    private $twig;

    public function __construct(
        Swift_Mailer $mailer,
        Twig_Environment $twig

    )
    {
        $this->mailer = $mailer;
        $this->twig = $twig;

    }


    public function sendConfirmationMessage(Users $user)
    {
        $messageBody = $this->twig->render('security/confirm.html.twig', [
            'user' => $user
        ]);

        $message = new Swift_Message();
        $message
            ->setSubject('Вы успешно прошли регистрацию!')
            ->setFrom(self::FROM_ADDRESS)
            ->setTo($user->getEmail())
                ->setBody($messageBody, 'text/html');

        $this->mailer->send($message);
    }
}
