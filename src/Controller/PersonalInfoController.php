<?php

namespace App\Controller;

use App\Entity\PersonalInfo;
use App\Entity\PersonalInfoTranslation;
use App\Form\PersonalInfoFormType;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\InfoTrans;

/**
 * @Route("/{_locale}")
 */
class PersonalInfoController extends AbstractController
{
    /**
     * @Route("/new", name="personal_info")
     */
    public function new(Request $request)
    {
        $info = new PersonalInfo();

        $form = $this->createForm(PersonalInfoFormType::class, $info);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $task = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($info);
            $entityManager->flush();

            return $this->redirectToRoute('allPersonal');
        }

        return $this->render('personal_info/NewPersonalInfo.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/ALLOFF ", name="allPersonal")
     */
    public function All()
    {
        $info = new PersonalInfo();
        $em = $this->getDoctrine()->getManager();
        $info = $em->getRepository(PersonalInfo::class)->findAll();

        return $this->render('personal_info/AllPersonalInfo.html.twig', [
            'PersonalInfo' => $info]);
    }

    /**
     * @Route("/EditPersonalInfo/{id}", name="edit_personal")
     */
    public function edit(Request $request, $id)
    {
        $info = new PersonalInfo();
        $info = $this->getDoctrine()->getRepository(PersonalInfo::class)->find($id);
        $form = $this->createFormBuilder($info)
            ->add('name', TextType::class)
            ->add('surname', TextType::class)
            ->add('age', TextType::class)
            ->add('save', SubmitType::class)
            ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();
        }

        return $this->render('personal_info/EditPersonalInfo.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/DeletePersonalInfo/{id}", name="delete_personal")
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function drop($id){
        $info= $this->getDoctrine()->getRepository(PersonalInfo::class)->find($id);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($info);
        $entityManager->flush();

        return $this->redirectToRoute('allTransl');
    }

    /**
     * @Route("/allTrans ", name="allTransl")
     */
    public function AlTrans()
    {
        $info = new PersonalInfo();
        $em = $this->getDoctrine()->getManager();
        $info = $em->getRepository(PersonalInfo::class)->findAll();

        return $this->render('personal_info/allTrans.html.twig', [
            'PersonalInfoTranslation' => $info]);
    }


    /**
     * @Route("/trans/new", name="trans")
     */
    public function personalInfoNew(Request $request)
    {
        $trans= new PersonalInfo();
        $form = $this->createForm(PersonalInfoFormType::class, $trans);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $trans = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($trans);
            $entityManager->flush();

            return $this->redirectToRoute('allTransl');
        }

        return $this->render('trans.html.twig',['form' => $form->createView()]);
    }
}
