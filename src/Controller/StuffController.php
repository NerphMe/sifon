<?php

namespace App\Controller;

use App\Entity\PersonalInfo;
use App\Entity\Stuff;
use App\Form\PersonalInfoFormType;
use App\Form\StuffType;
use FOS\RestBundle\Controller\Annotations as Rest;
use phpDocumentor\Reflection\Types\Integer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class StuffController extends AbstractController
{
    /**
     * @Route("/stuff", name="stuff")
     */
    public function index()
    {
        $stuff = new Stuff();
        $em = $this->getDoctrine()->getManager();
        $stuff = $em->getRepository(Stuff::class)->findAll();

        return $this->render('stuff/index.html.twig', [
            'Stuff' => $stuff,
        ]);

    }

    /**
     * @Route("/newStuff", name="newStuff")
     */
    public function newStuff(Request $request)
    {
        $stuff = new Stuff();

        $form = $this->createForm(StuffType::class, $stuff);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $stuff = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($stuff);
            $entityManager->flush();

            return $this->redirectToRoute('stuff');
        }

        return $this->render('stuff/newStuff.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/editStuff/{id}", name="editStuff")
     */
    public function editStuff(Request $request, $id)
    {
        $stuff = new Stuff();

        $form = $this->getDoctrine()->getRepository(Stuff::class)->find($id);
        $form = $this->createFormBuilder($form)
            ->add('name', TextType::class)
            ->add('surname', TextType::class)
            ->add('age', IntegerType::class)
            ->add('Save', SubmitType::class)
            ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();
            return $this->redirectToRoute('stuff');
        }
           return $this->render('stuff/editStuff.html.twig',['form' => $form->createView()]);
    }

    /**
     * @Route("/deleteStuff/{id}", name="deletestuff")
     */
    public function deleteStuff($id){
        $stuff= $this->getDoctrine()->getRepository(Stuff::class)->find($id);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($stuff);
        $entityManager->flush();

        return $this->redirectToRoute('stuff');
    }

}
