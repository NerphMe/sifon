<?php

namespace App\Controller;

use App\Entity\Users;
use App\Form\Type\UserType;
use App\Repository\UsersRepository;
use App\Service\CodeGenerator;
use App\Service\Mailer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGenerator;

class RegController extends Controller
{

    /**
     * @Route("/register", name="user_reg")
     */
    public function register(Request $request, Mailer $mailer, CodeGenerator $codeGenerator)
    {
        $user = new Users();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $password = password_hash($user->getPlainPassword(), PASSWORD_BCRYPT);
            $user->setPassword($password);
            $user->setConfirmationCode($codeGenerator->getConfirmationCode());

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
            $mailer->sendConfirmationMessage($user);

            return $this->redirectToRoute('app_login');
        }

        return $this->render(
            'security/reg.html.twig',
            ['form' => $form->createView()]
        );

    }

    /**
     * @Route("/confirm/{code}", name="confirm")
     */
    public function confirmEmail(UsersRepository $usersRepository, string $code)
    {

        $user = $usersRepository->findOneBy(['confirmationCode' => $code]);

        if ($user === null) {
            return new Response('404');
        }

        $user->setConfirmationCode('');

        $em = $this->getDoctrine()->getManager();

        $em->flush();

        return $this->render('security/PassUrAcc.html.twig', [
            'user' => $user,
        ]);

    }
}
