<?php

namespace App\Controller;

use App\Entity\Api;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class ApiForFrontController extends FOSRestController
{
    /**
     * @Rest\Get("/getApi")
     */
    public function ApiAll()
    {
        $repository = $this->getDoctrine()->getRepository(Api::class);
        /** @var Api[] $apis */
        $apis = $repository->findAll();
//        return $this->handleView($this->view($apis));

        $formatedapis = [];
        foreach ($apis as $api) {
            $formatedapi = [];
            $formatedapi['id'] = $api->getId();
            $formatedapi['name'] = $api->getName();
            $formatedapi['surname'] = $api->getSurname();
            $formatedapi['age'] = $api->getAge();

            $formatedapis[] = $formatedapi;
        }

        return new JsonResponse($formatedapis);
    }

    /**
     * @Rest\Post("/PostApi")
     */
    public function PostApi(Request $request)
    {
        $name = $request->get('name');
        $surname = $request->get('surname');
        $age = $request->get('age');
        $apis = new Api();
        $apis->setName($name);
        $apis->setSurname($surname);
        $apis->setAge($age);

        $manager = $this->getDoctrine()->getManager();
        $manager->persist($apis);
        $manager->flush();
        return new JsonResponse(
            'Copmleted');

    }

    /**
     * @Rest\Delete("/DeleteApi/{id}")
     * @param $id
     * @return JsonResponse
     */
    public function DeleteApi($id)
    {
        $api = $this->getDoctrine()->getRepository(Api::class)->find($id);
        $repository = $this->getDoctrine()->getManager();
        $repository->remove($api);
        $repository->flush();
        return new JsonResponse(
            'Deleted');
    }

    /**
     * @Rest\Patch("/putApi")
     * @param $id
     * @param Request $request
     * @return Response
     */
    public function updateApi(Request $request)
    {
        $name = $request->get('name');
        $surname = $request->get('surname');
        $age = $request->get('age');
        $id = $request->get('id');
        $api = $this->getDoctrine()->getRepository(Api::class)->find($id);
        $repository = $this->getDoctrine()->getManager();

        if ($api === null) {
            return new Response('Id was not found');
        }

        $api->setName($name);
        $api->setSurname($surname);
        $api->setAge($age);
        $repository->flush();

        return new Response('Edited');


    }
}
