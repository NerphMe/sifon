<?php

namespace App\Controller;

use App\Entity\PersonalInfo;
use App\Entity\Task;
use App\Form\PersonalInfoFormType;
use App\Form\Type\TaskType;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\UserBundle\Event\GetResponseGroupEvent;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Translation\Loader\ArrayLoader;
use Symfony\Contracts\Translation\TranslatorInterface;


/**
 * @Route("/{_locale}")
 */
class TaskController extends AbstractController
{

    /**
     * @Route("/task/new", name="task_new")
     */
    public function new(Request $request)
    {
        $task = new Task();

        $form = $this->createForm(TaskType::class, $task);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $task = $form->getData();   
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($task);
            $entityManager->flush();

            return $this->redirectToRoute('task_showAll');
        }

        return $this->render('task/new.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/tasks", name="task_showAll")
     */
    public function All()
    {
        $em = $this->getDoctrine()->getManager();
        $task = $em->getRepository(Task::class)->findAll();

        return $this->render('task/All.html.twig', [
            'tasks' => $task]);
    }

    /**
     * @Route("/task/Edit/{id}", name="edit_tasks")
     */
    public function edit(Request $request, $id)
    {
        $task = new Task();
        $repository = $this->getDoctrine()->getRepository(Task::class)->find($id);
        $form = $this->createFormBuilder($task)
            ->add('route', TextType::class)
            ->add('planeId', TextType::class)
            ->add('seatsAmmo', TextType::class)
            ->add('ticketPrice', TextType::class)
            ->add('dueDate', DateType::class)
            ->add('save', SubmitType::class)
            ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();
        }

        return $this->render('task/Edit.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("task/Delete/{id}", name="delete")
     */
    public function Remove($id)
    {
        $task = $this->getDoctrine()->getRepository(Task::class)->find($id);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($task);
        $entityManager->flush();

        return $this->redirectToRoute('task_showAll');
    }

    /**
     * @Route("task/{id}", name="task_show")
     */
    public function showAction($id)
    {
        $task = $this->getDoctrine()
            ->getRepository(Task::class)
            ->find($id);

        return $this->render('task/field.html.twig', ['task' => $task]);
    }


}
