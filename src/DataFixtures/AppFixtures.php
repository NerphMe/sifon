<?php

namespace App\DataFixtures;

use App\Entity\Task;
use App\Entity\Users;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    Private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 20; $i++) {
            $task = new Task();
            $task->setDueDate(new \DateTime());
            $task->setRoute('Daun');
            $manager->persist($task);
        }


            $add = new Users();
            $add->setUsername('user');
            $password = $this->encoder->encodePassword($add,'user');
            $add->setPassword($password);
            $add->setEmail('qwert@gmail.com');
            $add->setIsActive(true);
            $manager->persist($add);

            $manager->flush();
          }


}





