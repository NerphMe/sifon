<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\StuffRepository")
 */
class Stuff
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO" )
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $surname;

    /**
     * @ORM\OneToMany(targetEntity="SickDays",mappedBy="user",fetch="EAGER",cascade={"remove"})
     */
    private $sickDays;

    /**
     * @ORM\OneToMany(targetEntity="Vacation",mappedBy="user",fetch="EAGER",cascade={"remove"})
     */
    private $vacations;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $age;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(?string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function getAge(): ?int
    {
        return $this->age;
    }

    public function setAge(?int $age): self
    {
        $this->age = $age;

        return $this;
    }

    /**
     * @return \DateTime
     * @throws \Exception
     */
    public function getSickDays()
    {
        return new \DateTime();
    }

    /**
     * @param mixed $sickDays
     */
    public function setSickDays($sickDays): void
    {
        $this->sickDays = $sickDays;
    }

    /**
     * @param mixed $vacations
     */
    public function setVacations($vacations): void
    {
        $this->vacations = $vacations;
    }

    /**
     * @return \DateTime
     * @throws \Exception
     */
    public function getVacations()
    {
        return new \DateTime();

    }
}
