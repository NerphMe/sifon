<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;
use Symfony\Component\Validator\Constraints as Assert;
use ORMBehaviors\Translateble\Translateble;

/**
 * @ORM\Entity
 */
class Task
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @Gedmo\Translatable()
     * @ORM\Column(type="string")
     * @Assert\NotBlank
     */
    protected $route;


    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank
     */
    protected $planeId;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank
     */
    protected $seatsAmmo;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank
     */
    protected $ticketPrice;


    /**
     * @ORM\Column(type="datetime")
     * @Assert\NotBlank
     * @Assert\Type("\DateTime")
     */
    protected $dueDate;

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content): void
    {
        $this->content = $content;
    }

    /**
     * @Gedmo\Translatable()
     */
    private $content;

    /**
     * @Gedmo\Locale()
     */
    private $locale;


    public function setLocale($locale): void
    {
        $this->locale = $locale;
    }

    /**
     * @return mixed
     */
    public function getRoute()
    {
        return $this->route;
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $route
     */
    public function setRoute($route): void
    {
        $this->route = $route;
    }

    /**
     * @return mixed
     */
    public function getDueDate()
    {
        return $this->dueDate;
    }

    /**
     * @param mixed $dueDate
     */
    public function setDueDate($dueDate): void
    {
        $this->dueDate = $dueDate;
    }

    public function getRole()
    {
        return [
            'ROLE_USER',
        ];
    }

    public function getSalt()
    {
    }

    public function eraseCredentials()
    {
    }
    /**
     * @return mixed
     */
    public function getPlaneId()
    {
        return $this->planeId;
    }

    /**
     * @param mixed $planeId
     */
    public function setPlaneId($planeId): void
    {
        $this->planeId = $planeId;
    }

    /**
     * @return mixed
     */
    public function getSeatsAmmo()
    {
        return $this->seatsAmmo;
    }

    /**
     * @param mixed $seatsAmmo
     */
    public function setSeatsAmmo($seatsAmmo): void
    {
        $this->seatsAmmo = $seatsAmmo;
    }

    /**
     * @return mixed
     */
    public function getTicketPrice()
    {
        return $this->ticketPrice;
    }

    /**
     * @param mixed $ticketPrice
     */
    public function setTicketPrice($ticketPrice): void
    {
        $this->ticketPrice = $ticketPrice;
    }
}




