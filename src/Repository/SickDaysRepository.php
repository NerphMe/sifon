<?php

namespace App\Repository;

use App\Entity\SickDays;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method SickDays|null find($id, $lockMode = null, $lockVersion = null)
 * @method SickDays|null findOneBy(array $criteria, array $orderBy = null)
 * @method SickDays[]    findAll()
 * @method SickDays[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SickDaysRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SickDays::class);
    }

    // /**
    //  * @return SickDays[] Returns an array of SickDays objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SickDays
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
